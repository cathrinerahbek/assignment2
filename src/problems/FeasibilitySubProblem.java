package problems;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;
/**
 *
 * @author Cathrine Rahbek and Karoline Liisberg
 */
public class FeasibilitySubProblem {
    private final IloCplex model;
    private final IloNumVar p[][]; //variable: power output
    private final IloNumVar l[]; //variable: load shed
    private final IloNumVar v1[]; //slack variable v+
    private final IloNumVar v2[]; //slack variable v-
    private final IloNumVar q1[][]; //slack variable q+
    private final IloNumVar d2[][]; //slack variable d-
    private final IloNumVar b2[][]; //slack variable b-
    private final IloNumVar z2[][]; //slack variable z-
    private final UnitCommitmentProblem ucp;
    private final IloRange eConstraint[]; //ensures the power balance
    private final IloRange fConstraint[][]; //ensures that generators do not produce beyond their minimum capacity
    private final IloRange gConstraint[][]; //ensures that generators do not produce beyond their maximum capacity
    private final IloRange hConstraint[][]; //enforce the ramp-up limits
    private final IloRange iConstraint[][]; //enforce the ramp-down limits
    
    // We Create the LP model for the feasibility subproblem.
    // U is the solution to the MP and is fixed in the feasibility and optimality problem.
    public FeasibilitySubProblem(UnitCommitmentProblem ucp, double U[][]) throws IloException {
        this.ucp = ucp;
        
        // 1. Every model needs an IloCplex object
        this.model = new IloCplex();
        
        // 2. We create the decision variables p and l.
        p = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        l = new IloNumVar[ucp.getnHours()];
    
        for(int g = 1; g<= ucp.getnGenerators(); g++){
            for(int h = 1; h <= ucp.getnHours(); h++){
                p[g-1][h-1] = model.numVar(0, Double.POSITIVE_INFINITY);
            }
        }
        
        for(int h = 1; h <= ucp.getnHours(); h++){
            l[h-1] = model.numVar(0, Double.POSITIVE_INFINITY);
        }
        
        // We create the auxiliary variables v+, v-, q+, d-, b-, and z-.
        this.v1 = new IloNumVar[ucp.getnHours()];
        for(int h = 1; h<= ucp.getnHours(); h++){
            v1[h-1] = model.numVar(0, Double.POSITIVE_INFINITY);
        }
        
        this.v2 = new IloNumVar[ucp.getnHours()];
        for(int h = 1; h<= ucp.getnHours(); h++){
            v2[h-1] = model.numVar(0, Double.POSITIVE_INFINITY);
        }
        
        this.q1 = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int h = 1; h<= ucp.getnHours(); h++){
                q1[g-1][h-1] = model.numVar(0, Double.POSITIVE_INFINITY);
            }
        }
        
        this.d2 = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int h = 1; h<= ucp.getnHours(); h++){
                d2[g-1][h-1] = model.numVar(0, Double.POSITIVE_INFINITY);
            }
        }
        
        this.b2 = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int h = 1; h<= ucp.getnHours(); h++){
                b2[g-1][h-1] = model.numVar(0, Double.POSITIVE_INFINITY);
            }
        }
        
        this.z2 = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int h = 1; h<= ucp.getnHours(); h++){
                z2[g-1][h-1] = model.numVar(0, Double.POSITIVE_INFINITY);
            }
        }
        
        // 3. We create the objective function
        // We frist create an empty linear numerical expression
        IloLinearNumExpr obj = model.linearNumExpr();
        
        // We add the terms to the function
        for(int h = 1; h<= ucp.getnHours(); h++){
            obj.addTerm(1, v1[h-1]);
        }
        
        for(int h = 1; h<= ucp.getnHours(); h++){
            obj.addTerm(1, v2[h-1]);
        }
        
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int h = 1; h<= ucp.getnHours(); h++){
                obj.addTerm(1, q1[g-1][h-1]);
            }
        }
        
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int h = 1; h<= ucp.getnHours(); h++){
                obj.addTerm(1, d2[g-1][h-1]);
            }
        }
        
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int h = 1; h<= ucp.getnHours(); h++){
                obj.addTerm(1, b2[g-1][h-1]);
            }
        }
        
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int h = 1; h<= ucp.getnHours(); h++){
                obj.addTerm(1, z2[g-1][h-1]);
            }
        }
        
        // We tell cplex to minimize the objective function
        model.addMinimize(obj);
        
        // 4. We now create the constraints of the feasibility problem
        
        // Constraint (1e)
        this.eConstraint = new IloRange[ucp.getnHours()];
        for(int t = 1; t <= ucp.getnHours(); t++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                lhs.addTerm(1, p[g-1][t-1]);
            }
            lhs.addTerm(1, l[t-1]);
            eConstraint[t-1] = model.addEq(lhs, ucp.getDemand(t));
        }
        
        // Constraint (1f)
        this.fConstraint = new IloRange[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int h = 1; h <= ucp.getnHours(); h++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1, p[g-1][h-1]);
                lhs.addTerm(1, q1[g-1][h-1]);
                
                fConstraint[g-1][h-1] = model.addGe(lhs, ucp.getMinProduction(g)*U[g-1][h-1]);
            }
        }
        
        // Constraint (1g)
        this.gConstraint = new IloRange[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int h = 1; h <= ucp.getnHours(); h++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1, p[g-1][h-1]);
                lhs.addTerm(-1, d2[g-1][h-1]);
                
                gConstraint[g-1][h-1] = model.addLe(lhs, ucp.getMaxProduction(g)*U[g-1][h-1]);
            }
        }
        
        // Constraint (1h)
        this.hConstraint = new IloRange[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int h = 1; h <= ucp.getnHours(); h++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1, p[g-1][h-1]);
                lhs.addTerm(-1, p[g-1][h-2]);
                lhs.addTerm(-1, b2[g-1][h-1]);
                
                hConstraint[g-1][h-1] = model.addLe(lhs, ucp.getRampUp(g));
            }
        }
        
        // Constraint (1i)
        this.iConstraint = new IloRange[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int h = 1; h <= ucp.getnHours(); h++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1, p[g-1][h-2]);
                lhs.addTerm(-1, p[g-1][h-1]);
                lhs.addTerm(-1, z2[g-1][h-1]);
                
                iConstraint[g-1][h-1] = model.addLe(lhs, ucp.getRampDown(g));
            }
        }
        
    }
        
    // We solve the problem
    public void solve() throws IloException{
        model.setOut(null);
        model.solve();
    }
    
    // and return the objective value
    public double getObjective() throws IloException{
        return model.getObjValue();
    }
    
    // We first define and return the constant part of the feasibility cut.
    public double getCutConstant() throws IloException{
        double constant = 0;
        for(int h = 1; h <= ucp.getnHours(); h++){
            constant = constant + model.getDual(eConstraint[h-1]) * ucp.getDemand(h);
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                constant = constant + model.getDual(hConstraint[g-1][h-1]) * ucp.getRampUp(g);
                constant = constant + model.getDual(iConstraint[g-1][h-1]) * ucp.getRampDown(g);
            }
        }
        return constant;
    }
    
    // We then define and return the linear expression in u of the feasibility cut
    public IloLinearNumExpr getCutLinearTerm(IloIntVar u[][]) throws IloException{
        IloLinearNumExpr cutTerm = model.linearNumExpr();
        
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int h = 1; h <= ucp.getnHours(); h++){
                cutTerm.addTerm(model.getDual(fConstraint[g-1][h-1]) * ucp.getMinProduction(g), u[g-1][h-1]);
                cutTerm.addTerm(model.getDual(gConstraint[g-1][h-1]) * ucp.getMaxProduction(g), u[g-1][h-1]);
            }
        }
        return cutTerm;
    }
    
    public void end(){
        model.end();
    } 
}
