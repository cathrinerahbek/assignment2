package problems;

/**
 *
 * @author Cathrine Rahbeck and Karoline Liisberg
 */

public class UnitCommitmentProblem {
    //DEFINING THE FIELDS OF THE CLASS:
    // The state contains the data of the problem
    private final int nHours; // cardinality of the set T
    private final int nGenerators; // cardinality of the set G
    private final String[] generatorName; // set G
    private final double[] commitmentCost; // parameter: commitment cost for generator g
    private final double[] productionCost; // parameter: the marginal cost of producing with generator g
    private final double[] sheddingCost; // parameter: the loadshedding cost in period t
    private final double[] startUpCost; // parameter: the startup cost of generator g
    private final int[] minOnTime; // parameter: the minimum on-time for generator g
    private final int[] minOffTime; // parameter: minimum off-time for generator g
    private final int[][] minOnTimeAtT; // parameter: minimum on-time in period t
    private final int[][] minOffTimeAtT; // parameter: minimum off-time in period t
    private final double[] minProduction; // parameter: minimum power output for generator g
    private final double[] maxProduction; // parameter: maximum power output for generator g
    private final double[] rampUp; // parameter: ramp-up limit
    private final double[] rampDown; // parameter: ramp-down limit
    private final double[] demand; // power demand in period t
    
    //CREATING THE CONSTRUCTOR OF THE CLASS:    
    public UnitCommitmentProblem(int nHours, int nGenerators, String[] generatorName, 
            double[] commitmentCost, double[] productionCost, double[] sheddingCost, 
            double[] startUpCost, int[] minOnTime, int[] minOffTime, int[][] minOnTimeAtT, int[][] minOffTimeAtT, double[] minProduction, double[] maxProduction, double[] rampUp, double[] rampDown, double[] demand) {
        this.nHours = nHours;
        this.nGenerators = nGenerators;
        this.generatorName = generatorName;
        this.commitmentCost = commitmentCost;
        this.productionCost = productionCost;
        this.sheddingCost = sheddingCost;
        this.startUpCost = startUpCost;
        this.minOnTime = minOnTime;
        this.minOffTime = minOffTime;
        this.minOnTimeAtT = minOnTimeAtT;
        this.minOffTimeAtT = minOffTimeAtT;
        this.minProduction = minProduction;
        this.maxProduction = maxProduction;
        this.rampUp = rampUp;
        this.rampDown = rampDown;
        this.demand = demand;
    }
    
    //CREATING GETTERS:
    public int getnHours() {
        return nHours;
    }

    public int getnGenerators() {
        return nGenerators;
    }
    
    // We return the commitment cost for generator number g.
    public double getCommitmentCost(int g) {
        if(g < 1 || g > nGenerators){
            throw new IllegalArgumentException("The generator number must be in [1,"+nGenerators+"].");
        }
        return commitmentCost[g-1];
    }

    public double getProductionCost(int g) {
        if(g < 1 || g > nGenerators){
            throw new IllegalArgumentException("The generator number must be in [1,"+nGenerators+"].");
        }
        return productionCost[g-1];
    }
    
    // We return the shedding cost for a given hour h.
    public double getSheddingCost(int h) {
        System.out.println("h = "+h+ " "+nHours);
        if(h < 1 || h > nHours){
            throw new IllegalArgumentException("The hour number must be in [1,"+nHours+"].");
        }
        return sheddingCost[h-1];
    }

    public String getGeneratorName(int g){
        if(g < 1 || g > nGenerators){
            throw new IllegalArgumentException("The generator number must be in [1,"+nGenerators+"].");
        }
        return generatorName[g-1];
    }
    public double getStartUpCost(int g) {
        if(g < 1 || g > nGenerators){
            throw new IllegalArgumentException("The generator number must be in [1,"+nGenerators+"].");
        }
        return startUpCost[g-1];
    }

    public int getMinOnTime(int g) {
        if(g < 1 || g > nGenerators){
            throw new IllegalArgumentException("The generator number must be in [1,"+nGenerators+"].");
        }
        return minOnTime[g-1];
    }

    public int getMinOffTime(int g) {
        if(g < 1 || g > nGenerators){
            throw new IllegalArgumentException("The generator number must be in [1,"+nGenerators+"].");
        }
        return minOffTime[g-1];
    }
    
    // We return the minimum on-time for generator g at hour h.
    public int getMinOnTimeAtT(int g, int h) {
        if(h < 1 || h > nHours){
            throw new IllegalArgumentException("The hour number must be in [1,"+nHours+"].");
        }
        if(g < 1 || g > nGenerators){
            throw new IllegalArgumentException("The generator number must be in [1,"+nGenerators+"].");
        }
        return minOnTimeAtT[g-1][h-1];
    }

    public int getMinOffTimeAtT(int g, int h) {
        if(h < 1 || h > nHours){
            throw new IllegalArgumentException("The hour number must be in [1,"+nHours+"].");
        }
        if(g < 1 || g > nGenerators){
            throw new IllegalArgumentException("The generator number must be in [1,"+nGenerators+"].");
        }
        return minOffTimeAtT[g-1][h-1];
    }
    
    public double getMinProduction(int g) {
        if(g < 1 || g > nGenerators){
            throw new IllegalArgumentException("The generator number must be in [1,"+nGenerators+"].");
        }
        return minProduction[g-1];
    }

    public double getMaxProduction(int g) {
        if(g < 1 || g > nGenerators){
            throw new IllegalArgumentException("The generator number must be in [1,"+nGenerators+"].");
        }
        return maxProduction[g-1];
    }

    public double getRampUp(int g) {
        if(g < 1 || g > nGenerators){
            throw new IllegalArgumentException("The generator number must be in [1,"+nGenerators+"].");
        }
        return rampUp[g-1];
    }

    public double getRampDown(int g) {
        if(g < 1 || g > nGenerators){
            throw new IllegalArgumentException("The generator number must be in [1,"+nGenerators+"].");
        }
        return rampDown[g-1];
    }

    public double getDemand(int h) {
        if(h < 1 || h > nHours){
            throw new IllegalArgumentException("The hour number must be in [1,"+nHours+"].");
        }
        return demand[h-1];
    }   
}