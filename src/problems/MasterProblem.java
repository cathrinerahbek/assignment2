package problems;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;
/**
 *
 * @author Cathrine Rahbek and Karoline Liisberg
 */

/* 
We create the Master Problem using Bender's decomposition.
We recognize u as the complicating variable, since it is a binary variable, and
without it we will achieve a linear problem. We choose to also include the variable
c in the master problem, since this will ensure that the solution of the u's in 
the master problem will be better for solving the subproblems.
*/
public class MasterProblem {
    private final IloCplex model;
    private final IloIntVar u[][]; 
    private final IloNumVar c[][];
    private final IloNumVar phi;
    private final UnitCommitmentProblem ucp;
    private final IloRange[][] bConstraint; // Sets the stat-up cost when a generator is switched on
    private final IloRange[][] cConstraint; // Ensures that if a generator is strated up in period t it remains on for T^U periods.
    private final IloRange[][] dConstraint; // Ensures that if a generator is switched off in period t then it remains off for T^D periods.
    
    public MasterProblem(UnitCommitmentProblem ucp) throws IloException {
        this.ucp = ucp;
        
        // 1. Every model needs an IloCplex object
        this.model = new IloCplex();
        
        // 2. Creates the decision variables u, c and phi.
        this.u = new IloIntVar[ucp.getnGenerators()][ucp.getnHours()];
        this.c = new IloIntVar[ucp.getnGenerators()][ucp.getnHours()];
        
        for(int g = 1; g<= ucp.getnGenerators(); g++){
            for(int h = 1; h <= ucp.getnHours(); h++){
                u[g-1][h-1] = model.boolVar();
            }
        }
        
        for(int g = 1; g<= ucp.getnGenerators(); g++){
            for(int h = 1; h <= ucp.getnHours(); h++){
                c[g-1][h-1] = model.boolVar();
            }
        }
        
        this.phi = model.numVar(0, Double.POSITIVE_INFINITY,"phi");
        
        // 3. We create the objective function
        
        // Creates an empty linear numerical expression (linear equation)
        IloLinearNumExpr obj = model.linearNumExpr();
        
        // We add terms to the function
        for(int g = 1; g<= ucp.getnGenerators(); g++){
            for(int h = 1; h <= ucp.getnHours(); h++){
                obj.addTerm(1, c[g-1][h-1]);
            }
        }
        
        for(int g = 1; g<= ucp.getnGenerators(); g++){
            for(int h = 1; h <= ucp.getnHours(); h++){
                obj.addTerm(ucp.getCommitmentCost(g), u[g-1][h-1]);
            }
        }
        
        obj.addTerm(1, phi);
        
        // We Tell cplex to minimize the objective function
        model.addMinimize(obj);
        
        //We add constraints to the master problem
               
        // Constraint (1b)
        // We have one constraint for each hour and generator
        this.bConstraint = new IloRange[ucp.getnGenerators()][ucp.getnHours()];
        for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1, c[g-1][t-1]);
                // We bring the right-hand-side to the lhs, changing sign
                lhs.addTerm(-ucp.getStartUpCost(g), u[g-1][t-1]);
                if(t > 1){
                    lhs.addTerm(+ucp.getStartUpCost(g), u[g-1][t-2]); // Note that in order to get u_g,t-1 we need to access u[g-1][t-2] (notice the -2)                
                }
                // Finally we add the constraint
                bConstraint[g-1][t-1] = model.addLe(lhs,0);
            }
        }
        
        // Constraint (1c)
        this.cConstraint = new IloRange[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int h = 1; h <= ucp.getnHours(); h++){
                double T = Math.min(h + ucp.getMinOnTime(g)-1, ucp.getnHours());
                for(int t = h; t<= T; t++){
                    IloLinearNumExpr lhs = model.linearNumExpr();
                    lhs.addTerm(1, u[g-1][t-1]);
                    lhs.addTerm(-1, u[g-1][h-1]);
                    lhs.addTerm(1, u[g-1][h-2]);
                    
                    // we add the constraint
                    cConstraint[g-1][h-1] = model.addGe(lhs,0);
                }
            }
        }
        
        // Constraint (1d)
        this.dConstraint = new IloRange[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int h = 1; h <= ucp.getnHours(); h++){
                double T = Math.min(h + ucp.getMinOffTime(g)-1, ucp.getnHours());
                for(int t = h; t<= T; t++){
                    IloLinearNumExpr lhs = model.linearNumExpr();
                    lhs.addTerm(-1, u[g-1][t-1]);
                    lhs.addTerm(1, u[g-1][h-1]);
                    lhs.addTerm(-1, u[g-1][h-2]);
                    
                    // we add the constraint
                    dConstraint[g-1][h-1] = model.addGe(lhs,-T);
                }
            }
        }
    }
    
    // we solve the Master Problem.
    public void solve() throws IloException{
        
        // In this way we inform Cplex that we want to use the callback we define below
        model.use(new Callback());
        
        // Solves the problem
        model.solve();
    }
    // Defines the callback
    private class Callback extends IloCplex.LazyConstraintCallback{

        public Callback() {
        }
        
        @Override
        protected void main() throws IloException {
            // 1. Start by obtaining the solution at the current node
            double[][] U = getU();
            double Phi = getPhi();
            
            // 2. Check feasibility of the subproblem 
            // 2.1 We create and solve a feasibility subproblem 
            FeasibilitySubProblem fsp = new FeasibilitySubProblem(ucp,U);
            fsp.solve();
            double fspObjective = fsp.getObjective();
            
            // 2.2 We check if the suproblem is feasible (check if the objective is zero). 
            System.out.println("FSP "+fspObjective);
            if(fspObjective >= 0+1e-9){
                // 2.3 If the objective is positive the subproblem is not feasible.  
                // Hence we need a feasibility cut.
                System.out.println("Generating feasibility cut");
                // 2.4 We obtain the constant and the linear term of the cut
                // from the feasibility subproblem
                double constant = fsp.getCutConstant();
                IloLinearNumExpr linearTerm = fsp.getCutLinearTerm(u);
                
                // 2.5 Thus we generate and add a cut to the current model.
                add(model.le(linearTerm, -constant));
                
            }else{
                // 3. If the "if"-loop is not entered, the subproblem is feasible, hence we check optimality
                // and verify whether we should add an optimality cut or if the current solution is optimal.
                
                // 3.1. First, we create and solve an optimality suproblem
                OptimalitySubProblem osp = new OptimalitySubProblem(ucp,U);
                osp.solve();
                double ospObjective = osp.getObjective();
                
                // 3.2. Then we check if the optimality test is satisfied.
                System.out.println("Phi "+Phi+ " OSP "+ospObjective );
                if(Phi >= ospObjective - 1e-9){
                    // 3.3. In this case the problem at the current node
                    // is optimal.
                    System.out.println("The current node is optimal");
                }else{
                    // 3.4. In this case we need an optimality cut, since the current solution is not optimal. 
                    System.out.println("Generating optimality cut");
                    // We get the constant and the linear term from the optimality suproblem 
                    double cutConstant = osp.getCutConstant();
                    IloLinearNumExpr cutTerm = osp.getCutLinearTerm(u);
                    cutTerm.addTerm(-1, phi);
                    // and generate and add a cut. 
                    add(model.le(cutTerm, -cutConstant));
                }
            }
        }
        
        // The following returns the U component of the solution at the current B&B integer node
        // and it returns the U component of the current solution. 
        public double[][] getU() throws IloException{
           double U[][] = new double[ucp.getnGenerators()][ucp.getnHours()];
           for(int g = 1; g <= ucp.getnGenerators(); g++){
               for(int h = 1; h <= ucp.getnHours(); h++){
                   U[g-1][h-1] = getValue(u[g-1][h-1]);
               }
           }
           return U;
        }

        // In the following we return the value of phi at the current B&B integer node.
        public double getPhi() throws IloException{
           return getValue(phi);
        }
    }
    
    // We return the objective value
    public double getObjective() throws IloException{
        return model.getObjValue();
    }
    // We print the solution
    public void printSolution() throws IloException{
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int h = 1; h <= ucp.getnHours(); h++){
                System.out.println("U_"+g+h+" = "+model.getValue((u[g-1][h-1])));   
            }
        }
    }   
    public void end(){
        model.end();
    }
}